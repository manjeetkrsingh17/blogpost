## Backend (NodeJS | ExpressJS)

# Folder name : blogPost

## Setup instruction

# extract zip file

# set environment variable to development mode using this command

    export NODE_ENV="development"

# npm install

# npm run dev (It will run on 3000 port)

## Swagger link

http://localhost:3000/api-docs/v1/

## Frontend (ReactJS)

# Folder name: blogfrontend

## Setup instruction

# extract zip file

# npm install

# npm start
