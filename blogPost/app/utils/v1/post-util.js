import {
    post,
    get
} from "axios";
import config from "../../config";
import moment from 'moment';

export async function postList(skip, limit, category, tag) {
    try {
        const postURL = `${config.postApi}/v1.1/sites/${config.siteId}/posts?order=DESC&offset=${skip}&number=${limit}&category=${category}&tag=${tag}`;

        const response = await get(encodeURI(postURL));

        const posts = response.data;
        return posts;
    } catch (error) {
        console.log(error);
        throw new Error("Please try again later.");
    }
}

export async function categoryList(limit) {
    try {
        const categoryURL = `${config.postApi}/v1.1/sites/${config.siteId}/categories?order=DESC&number=${limit}`;

        const response = await get(categoryURL);

        const categories = response.data;
        return categories;
    } catch (error) {
        console.log(error);
        throw new Error("Please try again later.");
    }
}

export async function tagList(limit) {
    try {
        const tagURL = `${config.postApi}/v1.1/sites/${config.siteId}/tags?order=DESC&number=${limit}`;

        const response = await get(tagURL);

        const tags = response.data;
        return tags;
    } catch (error) {
        console.log(error);
        throw new Error("Please try again later.");
    }
}

export async function relatedPostList(limit, postId) {
    try {
        const postURL = `${config.postApi}/v1.1/sites/${config.siteId}/posts/${postId}/related`;

        const response = await post(postURL, {
            "size": limit
        });

        const posts = response.data;
        return posts;
    } catch (error) {
        console.log(error);
        throw new Error("Please try again later.");
    }
}

export async function getPost(postId) {
    try {
        const postURL = `${config.postApi}/v1.1/sites/${config.siteId}/posts/${postId}`;

        const response = await get(encodeURI(postURL));

        const post = response.data;
        return post;
    } catch (error) {
        console.log(error);
        throw new Error("Please try again later.");
    }
}

export function filterPostParams(post) {
    return {
        excerpt: post.excerpt,
        content: post.content,
        featured_image: post.featured_image,
        post_thumbnail: post.post_thumbnail ? post.post_thumbnail.URL : post.featured_image,
        createdAt: moment(post.date).fromNow(),
        author: post.author.name,
        _id: post.ID,
        title: post.title
    }
}