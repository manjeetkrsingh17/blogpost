import express from "express";
import winston from "winston";
import expressWinston from "express-winston";
import bodyParser from "body-parser";
import config from "./config";
import mongoose from "./config/db/mongoose";

import {
    success,
    successWithData,
    validationFailed,
    unauthorized,
    thirdPartyError,
    serverError,
    expiredSession,
    invalidSession,
} from "./utils/response";
import swaggerUi from "swagger-ui-express";

express.response.success = success;
express.response.successWithData = successWithData;
express.response.validationFailed = validationFailed;
express.response.unauthorized = unauthorized;
express.response.thirdPartyError = thirdPartyError;
express.response.serverError = serverError;
express.response.expiredSession = expiredSession;
express.response.invalidSession = invalidSession;

mongoose();

const app = express();

//version 1 docs

const swaggerDocumentv1 = require(process.cwd() +
    "/app/api-documentation/swaggerDoc.json");

var options = {
    explorer: true,
};

let swaggerHtmlV1 = swaggerUi.generateHTML(swaggerDocumentv1, options);

app.use("/api-docs/v1", swaggerUi.serveFiles(swaggerDocumentv1, options));
app.get("/api-docs/v1", (req, res) => {
    res.send(swaggerHtmlV1);
});

expressWinston.requestWhitelist.push("body");

app.use(
    expressWinston.logger({
        transports: [
            new winston.transports.Console({
                json: true,
                timestamp: true,
            }),
        ],
        meta: true,
        msg: "HTTP {{req.method}} {{req.url}}",
        expressFormat: true,
        colorize: false,
        ignoreRoute: function (req, res) {
            return false;
        },
    })
);

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, Content-Type, X-Access-Token, Content-Length, Accept"
    );
    res.header(
        "Cache-Control",
        "private, no-cache, no-store, must-revalidate, max-age=0"
    );
    res.header("Pragma", "no-cache");
    if ("OPTIONS" === req.method) {
        res.send(200);
    } else {
        next();
    }
});

app.use("/api/v1/blog", require("./blog/v1/router"));

app.use(
    expressWinston.errorLogger({
        transports: [
            new winston.transports.Console({
                json: true,
                colorize: true,
            }),
        ],
    })
);

app.listen(config.express.port, function (error) {
    if (error) {
        log.error("Unable to listen for connections", error);
        process.exit(10);
    }
    console.info("express is listening on port :" + config.express.port);
});