import config from "../../config";
import {
    getPosts,
    getCategories,
    getTags,
    getRelatedPosts,
    getPostById
} from "./controllers/post";

var router = require('express').Router();

router.get('/posts', getPosts);

router.get('/posts/:postId/related', getRelatedPosts);

router.get('/categories', getCategories);

router.get('/tags', getTags);

router.get('/post/:postId', getPostById)

module.exports = router