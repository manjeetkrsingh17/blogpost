import config from "../../../config";

import {
    postList,
    categoryList,
    tagList,
    relatedPostList,
    getPost,
    filterPostParams
} from "../../../utils/v1/post-util";

export async function getPosts(req, res) {
    let page = req.query.page;
    let category = req.query.category || "";
    let tag = req.query.tag || "";
    try {
        let skip = 0,
            limit = 25;

        if (page) {
            page = parseInt(page);
            skip = (page - 1) * limit;
        }

        let posts = await postList(skip, limit, category, tag);

        res.successWithData({
            count: posts.found,
            posts: posts.posts.map(post => {
                return filterPostParams(post)
            })
        });
    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getCategories(req, res) {
    try {

        const limit = 10;
        let categories = await categoryList(limit);

        res.successWithData({
            count: categories.found,
            categories: categories.categories.map(category => {
                return {
                    _id: category.ID,
                    name: category.name,
                    post_count: category.post_count
                }
            })
        });
    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getTags(req, res) {
    try {

        const limit = 5;
        let tags = await tagList(limit);

        res.successWithData({
            count: tags.found,
            tags: tags.tags.map(tag => {
                return {
                    _id: tag.ID,
                    name: tag.name,
                    post_count: tag.post_count
                }
            })
        });
    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getRelatedPosts(req, res) {
    const postId = req.params.postId;
    try {

        if (!postId) {
            return res.validationFailed("Post Id is missing.")
        }

        const limit = 5;

        let postIds = await relatedPostList(limit, postId);

        postIds = JSON.parse(JSON.stringify(postIds));

        const postPromises = postIds.hits.map(hit => {
            return getPost(hit.fields.post_id)
        })

        let posts = await Promise.all(postPromises);

        res.successWithData(posts.map(post => {
            return filterPostParams(post)
        }));
    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getPostById(req, res) {
    const postId = req.params.postId;
    try {

        if (!postId) {
            return res.validationFailed("Post Id is missing.")
        }
        let post = await getPost(postId);

        res.successWithData(filterPostParams(post));
    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}