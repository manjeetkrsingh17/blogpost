export const express = {
  port: process.env.EXPRESS_PORT || 8080
};

export const db = {
  url: "mongodb://localhost:27017/blog",
  user: '',
  password: ''
};

export const postApi = "https://public-api.wordpress.com/rest";