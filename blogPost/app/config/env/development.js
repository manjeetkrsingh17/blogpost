export const express = {
  port: process.env.EXPRESS_PORT || 3000
};

export const db = {
  url: "mongodb://localhost:27017/blog",
  user: "",
  password: ""
};

export const postApi = "https://public-api.wordpress.com/rest";

export const siteId = "107403796";