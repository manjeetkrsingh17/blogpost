import config from "../../config";
import mongoose from "mongoose";

mongoose.set("useCreateIndex", true);

export default function () {
  const db = mongoose.connect(config.db.url, {
    user: config.db.user,
    pass: config.db.password,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  const connection = mongoose.connection;


  return db;
}