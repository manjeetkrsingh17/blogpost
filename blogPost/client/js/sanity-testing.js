$(document).ready(function () {

  CanvasJS.addColorSet("colorShades",
    [ //colorSet Array
      "#00876c",
      "#4d9f6c",
      "#81b56c",
      "#b6c96f",
      "#eeda7a",
      "#efb65d",
      "#ec904e",
      "#e4684b",
      "#d43d51"
    ]);

  let baseUrl = "/api/v1/";
  // $("#sidebar").toggleClass("active");
  var token;
  var activeTable;
  if (window.location.pathname !== "/" && localStorage.getItem("data")) {
    token = localStorage.getItem("data").token;
    let expiry = localStorage.getItem("data").timestamp;

    if (new Date().getTime() - expiry >= 3600000) {
      localStorage.removeItem("data");
      window.location.href = "/";
    }
  } else if (window.location.pathname !== "/") {
    window.location.href = "/";
  }

  var rules = [{
      text: "Rule 1",
      id: "rule1",
      description: "Check for an active assessment, question count should be greater than 0.",
      headers: "assessmentKey,assessmentName"
    },
    {
      text: "Rule 2",
      id: "rule2",
      description: "For all active assessments corresponding related KSA and Age, there should be minimum 1 activity.",
      headers: "age,assessmentKey,assessmentName,KSAKey,KSAName"
    },
    {
      text: "Rule 3",
      id: "rule3",
      description: "Each question should be link with indicator",
      headers: "questionKey,questionName"
    },
    {
      text: "Rule 4",
      id: "rule4",
      description: "For all active cases, insight count should be greater than 0",
      headers: "caseKey,caseName"
    },
    {
      text: "Rule 5",
      id: "rule5",
      description: "For every age group and for each case, there should be minimum 1 activity",
      headers: "age,caseKey,caseName"
    },
    {
      text: "Rule 6",
      id: "rule6",
      description: "For every active activity should have minimum 1 ksa",
      headers: "activityKey,activityName"
    },
    {
      text: "Rule 7",
      id: "rule7",
      description: "For every active activity should have minimum 1 indicator",
      headers: "activityKey,activityName"
    },
    {
      text: "Rule 8",
      id: "rule8",
      description: "For every active assessment for age 3-16 yrs, there should be minimum 1 KSA",
      headers: "assessmentKey,assessmentName"
    },
    {
      text: "Rule 9",
      id: "rule9",
      description: "For every active assessment for age 3-16 yrs, there should be area's impacted",
      headers: "assessmentKey,assessmentName"
    },
    {
      text: "Rule 10",
      id: "rule10",
      description: "For every active assessment for age 0-3 yrs, there should be minimum 1 milestone",
      headers: "assessmentKey,assessmentName"
    },
    {
      text: "Rule 11",
      id: "rule11",
      description: "For all table check any duplicate key exists or not.",
      headers: "key,collectionName,count"
    }
  ];

  function compare(a, b) {
    if (a.y < b.y) {
      return 1;
    } else {
      return -1;
    }
  }

  function sort_li(a, b) {
    return ($(b).data('position')) > ($(a).data('position')) ? 1 : -1;
  }

  function tableData(activeTable) {
    if (localStorage.getItem("data")) {
      let expiry = JSON.parse(localStorage.getItem("data")).timestamp;

      if (new Date().getTime() - expiry >= 3600000) {
        localStorage.removeItem("data");
      }
    } else {
      window.location.href = "/";
    }

    $(".content-heading").css("display", "none");

    // $("ul#tableHeaders > li").css("background", "#0957A5");

    // $("ul#tableHeaders > li")
    //   .find("span.tab")
    //   .css("color", "white");

    $("#mainTableContent")
      .children()
      .css("display", "none");

    $("section#" + activeTable + "").css("display", "block");
  }

  $.ajax({
    url: baseUrl + "sanity/testing",
    beforeSend: function () {
      $('#refresh').attr("disabled", true);
      $('#loader').show();
    },
    complete: function () {
      $('#loader').hide();
      $('#refresh').attr("disabled", false);
    },
    success: function (data) {
      let datapoints = [];
      let count = data.count;

      for (let i = 0; i < rules.length; i++) {
        let ruleBugCount = data[rules[i].id] ? data[rules[i].id].length : 0;

        $('.issueCount').text(data.count)

        $("ul#tableHeaders").append(
          '<li data-position="' + ruleBugCount + '" id="' +
          rules[i].id +
          '" data-toggle="tooltip" data-placement="right" title="' +
          rules[i].description +
          '"><a href="#' +
          rules[i].id +
          '"><div class="card-body"><h5 class="card-title"><strong>' +
          rules[i].text +
          '</strong><span class="ruleBugCount pull-right">' +
          ruleBugCount +
          '</span></h5><span class="small">' +
          rules[i].description +
          "</span></div></li>"
        );

        $("div#mainTableContent").append(
          '<section id="' +
          rules[i].id +
          '" class="tableContent"><h5 class="" style="padding-bottom:10px;"> Description : ' +
          rules[i].description +
          '</h4><table id="' +
          rules[i].id +
          'Table" class="table table-hover table-bordered display cmsTable"></table></section>'
        );

        if (ruleBugCount !== 0) {
          datapoints.push({
            label: rules[i].text,
            y: ruleBugCount
          });

          $("#" + rules[i].id + "Table > tbody tr").empty();

          let columnHeads = [];
          let columnDefs = [{
            defaultContent: "-",
            targets: "_all"
          }];

          for (let j = 0; j < rules[i].headers.split(",").length; j++) {
            columnHeads.push({
              title: rules[i].headers.split(",")[j],
              data: rules[i].headers.split(",")[j]
            });
          }

          $("#" + rules[i].id + "Table").DataTable({
            data: data[rules[i].id] ? data[rules[i].id] : [],
            columns: columnHeads,
            columnDefs: columnDefs
          });
        } else {
          $("section#" + rules[i].id).append('<div class="loading-image"><img src="./image/tickmark.svg" height="80" width="80" /><br/><label class="allgood">All Good.</label></div>')
        }
      }

      $("#tableHeaders li").sort(sort_li) // sort elements
        .appendTo('#tableHeaders');

      $("#mainTableContent")
        .children()
        .css("display", "none");

      $("#tableHeaders li").click(function (e) {
        e.preventDefault();
        activeTable =
          $(this)
          .closest("li")
          .attr('id')

        $("ul#tableHeaders > li").css("background", "#0957A5");

        $("ul#tableHeaders > li").css("color", "white");

        $(this)
          .closest("li")
          .css("background", "white");

        $(this)
          .closest("li")
          .css("color", "#0957A5");

        tableData(activeTable);
      });

      datapoints = datapoints.sort(compare)
      console.log(datapoints)
      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        // title: {
        //   text: "Sanity Testing Issues"
        // },
        colorSet: "colorShades",
        data: [{
          indexLabelPlacement: "inside",
          indexLabelFormatter: function (e) {
            return e.dataPoint.label + " (" + e.dataPoint.y + ")";
          },
          type: "pie",
          startAngle: 90,
          toolTipContent: "{label}: <strong>({y})</strong>",
          //yValueFormatString: "##0.00\"%\"",
          indexLabel: "{label} {y}",
          dataPoints: datapoints
        }]
      });
      chart.render();
    },
    error: function (jqxhr) {
      console.log(jqxhr.responseText);
      alert(jqxhr.responseText);
    }
  });

  $("#sidebarCollapse").on("click", function () {
    $("#sidebar").toggleClass("active");
  });

  $("#logout").click(function (e) {
    localStorage.removeItem("data");
    window.location.href = "/";
  });

  $("#refresh").click(function (e) {
    $("#bodyContent").load("sanity-testing");

    return false;
  });

  $("#ruleLabel").click(function (e) {
    $("#mainTableContent")
      .children()
      .css("display", "none");

    $('.content-heading').css("display", "block");

    $("ul#tableHeaders > li").css("background", "#0957A5");

    $("ul#tableHeaders > li").css("color", "white");

    // $("#bodyContent").load("sanity-testing");
    // return false;
  });
});