import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import Post from "./Home/Post";
import Home from "./Home/Home";
import history from "./history";

export default class Routes extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Home} />
          {/* <Route path="/post-detail/:id" component={Post} /> */}
          <Route path="/post-detail" component={Post} />
        </Switch>
      </Router>
    );
  }
}
