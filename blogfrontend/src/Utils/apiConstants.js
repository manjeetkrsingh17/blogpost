export const GET_POST_LIST = "/v1/blog/posts";
export const GET_CATEGORY_LIST = "/v1/blog/categories";
export const GET_TAG_LIST = "/v1/blog/tags";
export const GET_POST = "/v1/blog/post";
export const GET_RELATED_POST = "/v1/blog/posts/{postId}/related"