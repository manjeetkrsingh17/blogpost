import React from "react";
import history from "./../history";
import { Link } from "react-router-dom";

export const Tags = (props) => {
  const { tagData } = props;
  return (
    <div className="card my-4">
      <h5 className="card-header">Tags</h5>
      <div className="card-body">
        <div className="row">
          <div className="col-lg-8">
            <ul className="list-unstyled mb-0">
              {tagData.tags.map((tag, i) => (
                <li className="col-border" key={i}>
                  <Link
                    to={{
                      pathname: `/`,
                      state: { tag: tag.name },
                    }}
                  >
                    {tag.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
