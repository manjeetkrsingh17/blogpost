import React from "react";
import history from "./../history";
import { Link } from "react-router-dom";

export const RelatedPost = (props) => {
  console.log(props);
  const { post } = props;
  if (post) {
    return (
      <div className="card my-4">
        <h5 className="card-header">Popular Post</h5>
        <div className="card-body">
          <div className="row">
            <div className="col-lg-12">
              <ul className="list-unstyled mb-0">
                {post.data.map((p, i) => (
                  <li className="col-border" key={i}>
                    <Link
                      to={{
                        pathname: `/post-detail`,
                        search: `?id=${p._id}`,
                        state: { post: p },
                      }}
                    >
                      {p.title}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return "";
  }
};
