import React from "react";
import history from "./../history";
import { Link } from "react-router-dom";

export const Category = (props) => {
  const { categoryData } = props;
  return (
    <div className="card my-4">
      <h5 className="card-header">Categories</h5>
      <div className="card-body">
        <div className="row">
          <div className="col-lg-8">
            <ul className="list-unstyled mb-0">
              {categoryData.data.categories.map((cat, i) => (
                <li className="col-border" key={i}>
                  <Link
                    to={{
                      pathname: `/`,
                      state: { category: cat.name },
                    }}
                  >
                    {cat.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
