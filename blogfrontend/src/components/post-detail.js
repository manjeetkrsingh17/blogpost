import React from "react";
import history from "./../history";
import { Link } from "react-router-dom";

export const PostDetail = (props) => {
  //console.log(props);
  const { post } = props;
  return (
    <div>
      <h4 className="mt-4">{post.title}</h4>
      <hr />
      <p>{post.createdAt}</p>
      <hr />

      <img
        className="img-fluid rounded"
        src={post.featured_image}
        alt={post.title}
      />
      <hr />

      <p
        dangerouslySetInnerHTML={{
          __html: post.content,
        }}
      ></p>

      <hr />
    </div>
  );
};
