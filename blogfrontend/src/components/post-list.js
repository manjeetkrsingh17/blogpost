import React from "react";
import history from "./../history";
import { Link } from "react-router-dom";

export const POST = (props) => {
  //console.log(props);
  const { postData } = props;
  if (postData.posts.length) {
    return (
      <div>
        {postData.posts.map((post, i) => (
          <div className="card mb-4" key={i}>
            <img
              className="card-img-top"
              src={post.featured_image}
              alt="Card image cap"
            />
            <div className="card-body">
              <h4 className="card-title">{post.title}</h4>
              <div
                className="card-text"
                dangerouslySetInnerHTML={{
                  __html: post.excerpt,
                }}
              />

              <Link
                className="btn btn-primary"
                to={{
                  pathname: `/post-detail`,
                  search: `?id=${post._id}`,
                  state: { post: post },
                }}
              >
                Read More
              </Link>
            </div>
            <div className="card-footer text-muted">
              {post.createdAt} by {post.author}
            </div>
          </div>
        ))}
      </div>
    );
  } else {
    return <div>Posts are not available under this category.</div>;
  }
};
