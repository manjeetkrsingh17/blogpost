import React from "react";
import { withRouter } from "react-router-dom";
import { PostDetail } from "./../components/post-detail";
import { Tags } from "./../components/tags";
import { Category } from "./../components/category";
import { RelatedPost } from "./../components/related-post";
import axios from "axios";

import { config } from "./../Utils/config";
import { GET_RELATED_POST } from "./../Utils/apiConstants";

class App extends React.Component {
  constructor(props) {
    super(props);

    let search = window.location.search;
    let params = new URLSearchParams(search);
    let postId = params.get("id");

    this.state = {
      error: null,
      post: this.props.location.state.post,
      postId: postId,
    };
  }

  loadRelatedPost() {
    let RELATED_POST_API = `${config.dev}${GET_RELATED_POST}`.replace(
      "{postId}",
      this.state.postId
    );

    const postPromise = axios.get(RELATED_POST_API);

    axios
      .all([postPromise])
      .then(
        axios.spread((...responses) => {
          const relatedPosts = responses[0];
          console.log("relatedPosts", relatedPosts.data);
          this.setState({
            relatedPosts: relatedPosts.data,
          });
        })
      )
      .catch((errors) => {
        this.setState({ error: errors });
      });
  }

  componentDidMount() {
    this.loadRelatedPost();
  }

  render() {
    const { error, post, relatedPosts } = this.state;
    if (post) {
      return (
        <div className="App">
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <PostDetail post={post} />
              </div>
              <div className="col-md-4">
                <RelatedPost post={relatedPosts} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return <div>Loading...</div>;
    }
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;

    let search = window.location.search;
    let params = new URLSearchParams(search);
    let postId = params.get("id");

    this.state = {
      post: this.props.location.state.post,
      postId: postId,
    };

    this.loadRelatedPost();
  }
}

export default withRouter(App);
