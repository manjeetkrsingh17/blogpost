import React from "react";
import { POST } from "./../components/post-list";
import { Tags } from "./../components/tags";
import { Category } from "./../components/category";

import { config } from "./../Utils/config";
import {
  GET_POST_LIST,
  GET_CATEGORY_LIST,
  GET_TAG_LIST,
} from "./../Utils/apiConstants";

import axios from "axios";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      postData: {},
      tagData: {},
      categoryData: {},
      tagId: null,
      categoryId: null,
    };
  }

  loadPostCategoryTags() {
    let POST_API = `${config.dev}${GET_POST_LIST}`;
    let CATEGORY_API = `${config.dev}${GET_CATEGORY_LIST}`;
    let TAG_API = `${config.dev}${GET_TAG_LIST}`;

    if (this.state.tagId) {
      POST_API = `${POST_API}?tag=${this.state.tagId}`;
    }

    if (this.state.categoryId) {
      POST_API = `${POST_API}?category=${this.state.categoryId}`;
    }

    const postPromise = axios.get(POST_API);
    const categoryPromise = axios.get(CATEGORY_API);
    const tagPromise = axios.get(TAG_API);

    axios
      .all([postPromise, categoryPromise, tagPromise])
      .then(
        axios.spread((...responses) => {
          const posts = responses[0];
          const categories = responses[1];
          const tags = responses[2];

          this.setState({
            postData: posts.data,
            tagData: tags.data,
            categoryData: categories.data,
          });
        })
      )
      .catch((errors) => {
        this.setState({ error: errors });
      });
  }

  componentDidMount() {
    this.loadPostCategoryTags();
  }

  render() {
    const { error, postData, tagData, categoryData } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      if (postData && postData.data && postData.data.posts) {
        return (
          <div className="App">
            <div className="container">
              <div className="row">
                <div className="col-md-8">
                  <POST postData={postData.data} />
                </div>
                <div className="col-md-4">
                  <Tags tagData={tagData.data} />
                  <Category categoryData={categoryData} />
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return <div>Loading...</div>;
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;

    if (
      this.props &&
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.tag
    ) {
      this.state = {
        tagId: this.props.location.state.tag,
        categoryId: null,
      };

      this.loadPostCategoryTags();
    } else if (
      this.props &&
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.category
    ) {
      this.state = {
        categoryId: this.props.location.state.category,
        tagId: null,
      };
      this.loadPostCategoryTags();
    }
  }
}

export default App;
